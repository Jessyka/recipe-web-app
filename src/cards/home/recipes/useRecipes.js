import {useState, useEffect} from 'react';
import {API_URL} from '../../../config';
import AlertSeverity from "../../../components/alertMessage/AlertSeverity";

export const useRecipes = searchData => {
    const [recipes, setRecipes] = useState([]);
    const [alertMessage, setAlertMessage] = useState({
        message: '',
        showMessage: false,
        severity: AlertSeverity.error
    });

    const addAlert = (message, severity = AlertSeverity.error) => {
        setAlertMessage({
            message,
            showMessage: true,
            severity
        });
        setRecipes([]);
    }

    useEffect(() => {
        if (searchData.length) {
            fetch(API_URL(searchData))
                .then(response => response.json().then(({hits}) => {
                    if (hits.length) {
                        setRecipes(hits);
                        setAlertMessage({
                            message: '',
                            showMessage: false,
                            severity: AlertSeverity.error
                        });
                        return;
                    }
                    addAlert('Nenhum item encontrado.', AlertSeverity.info);
                }))
                .catch(() => addAlert('Error ao carregar receitas.'));
        }
    }, [searchData]);

    return {recipes, alertMessage};
}
